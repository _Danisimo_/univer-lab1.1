public class Server extends Computer {
    private int RAM ;
    private int HDD ;
    private int CPU ;

    public PC(int RAM , int HDD , int CPU ){
        this.RAM = RAM;
        this.HDD = HDD;
        this.CPU = CPU;


    }

    @Override
    public int getRAM(){
        return this.RAM;
    }

    @Override
    public int getHDD(){
        return this.HDD;
    }

    @Override
    public int getCPU(){
        return this.CPU;
    }
